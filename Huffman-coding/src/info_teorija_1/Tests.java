/*
 *  Copyright 2013 Povilas Versockas
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package info_teorija_1;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.junit.Test;

import com.google.common.base.Stopwatch;

public class Tests {
	public static void test(String fileIn, String fileOut, String fileRes)
			throws IOException {
		Stopwatch total = Stopwatch.createStarted();
		for (int i = 2; i <= 16; i++) {
			File in = new File(fileIn);
			File out = new File(fileOut);
			File res = new File(fileRes);

			System.out.println("Testing with " + i);

			Encode.clear();
			Decode.clear();
			HuffmanCode.clear();

			Stopwatch encode = Stopwatch.createStarted();
			Encode.encode(i, in, out);
			System.out.println("Encoding: "
					+ encode.elapsed(TimeUnit.MILLISECONDS));
			encode.stop();
			Stopwatch decode = Stopwatch.createStarted();

			Decode.decode(out, res);
			System.out.println("Decoding: "
					+ decode.elapsed(TimeUnit.MILLISECONDS));

			assertTrue(FileUtils.contentEquals(in, res));
			System.out.println("completed");
			out.delete();
			res.delete();
		}
		System.out.println("Total test: "
				+ total.elapsed(TimeUnit.MILLISECONDS));
	}

	@Test
	public void test1() throws IOException {
		test("kodai_sifrai.pdf", "test1OUT", "test1.pdf");
	}

	@Test
	public void test2() throws IOException {
		test("2012 Mokejimu istatymas.doc", "test2OUT", "test2.doc");
	}

	@Test
	public void test3() throws IOException {
		test("JLMassey_I.pdf", "test3OUT", "test3.pdf");
	}

	@Test
	public void test4() throws IOException {
		test("2012 Vartotoj� teisi� apsaugos �statymas.doc", "test4OUT",
				"test4.doc");
	}

}
