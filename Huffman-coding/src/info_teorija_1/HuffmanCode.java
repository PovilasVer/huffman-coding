/*
 * Code taken from rosettacode.org and edited:
 * Added EncodeTable/DecodeTable
 * Also creating HuffmanNode doesn't need Huffman leafs to have frequency
 */
package info_teorija_1;

import java.util.Hashtable;
import java.util.Map;
import java.util.PriorityQueue;

abstract class HuffmanTree implements Comparable<HuffmanTree> {
	public final int frequency; // the frequency of this tree
	public HuffmanTree father;

	public HuffmanTree(int freq) {
		frequency = freq;
	}

	@Override
	public int compareTo(HuffmanTree tree) {
		return frequency - tree.frequency;
	}
}

class HuffmanLeaf extends HuffmanTree {
	public final int value;

	public HuffmanLeaf(int freq, int val) {
		super(freq);
		value = val;
	}
}

class HuffmanNode extends HuffmanTree {
	public HuffmanTree left, right;

	public HuffmanNode(HuffmanTree l, HuffmanTree r) {
		super((l == null || r == null) ? 0 : (l.frequency + r.frequency));
		left = l;
		right = r;
	}
}

public class HuffmanCode {
	public static Map<Integer, String> encodeTable = new Hashtable<Integer, String>();
	public static Map<String, Integer> decodeTable = new Hashtable<String, Integer>();

	public static void clear() {
		encodeTable = new Hashtable<Integer, String>();
		decodeTable = new Hashtable<String, Integer>();
	}

	public static HuffmanTree buildTree(int[] byteFreqs) {
		PriorityQueue<HuffmanTree> trees = new PriorityQueue<HuffmanTree>();
		for (int i = 0; i < byteFreqs.length; i++)
			if (byteFreqs[i] > 0)
				trees.offer(new HuffmanLeaf(byteFreqs[i], i));

		assert trees.size() > 0;

		while (trees.size() > 1) {
			HuffmanTree a = trees.poll();
			HuffmanTree b = trees.poll();
			HuffmanNode newNode = new HuffmanNode(a, b);
			a.father = newNode;
			b.father = newNode;
			trees.offer(newNode);
		}
		return trees.poll();
	}

	public static void genCodes(HuffmanTree tree, StringBuffer prefix) {
		assert tree != null;

		if (tree instanceof HuffmanLeaf) {
			HuffmanLeaf leaf = (HuffmanLeaf) tree;
			// System.out.println(leaf.value +"\t"+ leaf.frequency + "\t" +
			// prefix);
			encodeTable.put(leaf.value, prefix.toString());
			decodeTable.put(prefix.toString(), leaf.value);

		} else if (tree instanceof HuffmanNode) {
			HuffmanNode node = (HuffmanNode) tree;

			prefix.append('0');
			genCodes(node.left, prefix);
			prefix.deleteCharAt(prefix.length() - 1);

			prefix.append('1');
			genCodes(node.right, prefix);
			prefix.deleteCharAt(prefix.length() - 1);
		}
	}

}