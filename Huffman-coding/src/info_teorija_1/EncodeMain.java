/*
 *  Copyright 2013 Povilas Versockas
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package info_teorija_1;

import java.io.File;
import java.io.IOException;

public class EncodeMain {

	public static void main(String[] args) {
		if (args.length != 3) {
			System.out.println("Usage: input_file output_file word_length");
			return;
		}

		File in = new File(args[0]);
		File out = new File(args[1]);
		int wordLength = Integer.parseInt(args[2]);

		try {
			Encode.encode(wordLength, in, out);
		} catch (IOException e) {
			System.out.println("Error:" + e);
		}
	}

}
