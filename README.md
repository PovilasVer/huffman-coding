Huffman-coding
==============

Java implementation of Huffman coding file compression.

Uses [jinahya](https://jinahya.com/mvn/site/com.googlecode.jinahya/bit-io/1.0-alpha-13-SNAPSHOT/apidocs/com/googlecode/jinahya/io/package-summary.html) and [guava](https://code.google.com/p/guava-libraries/)

More info [here](http://versockas.blogspot.com/2013/12/huffman-coding-implementation.html)

###You can compile it with:
javac src\info_teorija_1\*.java -cp *.jar -d classes

###run:
java info_teorija_1.EncodeMain %1 %2 %3
